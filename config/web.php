<?php
/**
 * 系统配置文件
 */
return [
	'title' => 'HuoCMS官网',
	'key' => 'cms,官网',
	'desc' => '免费好用的HuoCMS',
	'tel' => '400-855-0210',
	'down_url' => '',
	'qq' => '',
	'mail' => 'shuqi@suq.cn',
	'addr' => '江苏省南京市雨花台区天溯科技园1栋502',
	'beian' => '苏ICP备2021024265号',
	'subscribe' => '',
	'scan' => '',
	'tongji' => '',
	'logo' => '',
	'bg' => '',
	'login_captcha' => '0',
	'smtp-user' => '',
	'smtp-pass' => '',
	'smtp-port' => '',
	'smtp-host' => '',
	'file-type' => '1',
	'file-endpoint' => '',
	'file-OssName' => '',
	'file-accessKeyId' => '',
	'file-accessKeySecret' => '',
];