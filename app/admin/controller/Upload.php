<?php

namespace app\admin\controller;

class Upload extends Base
{
    //上传图片
    public function img()
    {
        $file = request()->file('file');
        // 检测图片格式
        $ext = $file->getOriginalExtension();

        $extArr = explode('|', 'jpg|png|gif|jpeg');
        if(!in_array($ext, $extArr)){
            return json(['code' => -3, 'data' => '', 'msg' => '只能上传jpg|png|gif|jpeg的文件']);
        }

        // 移动到框架应用根目录/public/uploads/ 目录下
        $saveName = \think\facade\Filesystem::disk('public')->putFile('', $file);

        return jsonReturn(0, '上传成功', ['src' => '/upload/' . $saveName]);
    }
}