<?php
declare (strict_types = 1);

namespace app\admin\controller\admin;

use think\facade\Request;
use app\common\service\AdminDepartment as S;
use app\common\model\AdminDepartment as M;

class Department extends  \app\admin\controller\Base
{
    protected $middleware = ['AdminCheck','AdminPermission'];

    // 列表
    public function index()
    {
        if (Request::isAjax()) {
            return json(M::getList());
        }
        return $this->fetch();
    }

    // 添加
    public function add()
    {
        if (Request::isAjax()) {
            return $this->getJson(S::goAdd(Request::post()));
        }

        $pid = input('param.parent_id');
        if ($pid == 0) {
            $name = '顶级部门';
        } else {
            $info = M::where('dept_id', $pid)->find();
            $name = $info['name'];
        }

        return $this->fetch('', [
            'parent_id' => $pid,
            'name' => $name
        ]);
    }

    // 编辑
    public function edit($id)
    {
        if (Request::isAjax()) {
            return $this->getJson(S::goEdit(Request::post(),$id));
        }
        return $this->fetch('',['model' => M::where('dept_id', $id)->find()]);
    }

    // 删除
    public function remove($id)
    {
        return $this->getJson(S::goRemove($id));
    }

}
