<?php

namespace app\common\model;

use think\Model;

class AdminUpdateLog extends Model
{
    /**
     * 获取更新日志列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getLogList($where, $limit)
    {
        try {

            $list = $this->where($where)->order('log_id','desc')->where('is_delete', 1)->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0,'success', $list);
    }
}